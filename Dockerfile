# You should always specify a full version here to ensure all of your developers
# are running the same version of Node.
FROM node:8.4.0

CMD echo node -v

WORKDIR /var/www/

# Install all dependencies of the current project.
COPY package.json package.json
RUN rm -rf node_modules
RUN npm install

RUN npm install pm2 -g

# Copy all local files into the image.
COPY . .

# Expose the public http port
EXPOSE 80
EXPOSE 3000
EXPOSE 9229

# Start server
# Start the service
WORKDIR /var/www/src
CMD ["npm", "start"]
