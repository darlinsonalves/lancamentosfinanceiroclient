import * as type from '../types/usuario'

export default (state = type.INITIAL_STATE, action, usuario = {}) => {
    switch (action.type) {
        case type.LOADING_USER:
            return {
                ...state,
                loading: true
            }
        case type.GET_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                usuario: action.payload.usuario,
            }
        case type.GET_USER_ERROR:
            return {
                ...state,
                loading: false
            }
        case type.LOGOUT:
            return {
                ...state,
                loading: false,
                usuario: null 
            }
        default:
            return {
                ...state
            }
    }
}