/* Dependencies */
import React, { Component, Fragment } from 'react'
import { LancamentoItem } from './lancamentoItem';
export class LancamentoList extends Component {
	componentWillMount(){
        this.props.getLancamentos()
    }

	render() {
		const {lancamentos, history}	= this.props
		if (localStorage.getItem('conta') === null) {
            history.push('/')
        }
		return (
			<section className="account-config container-fluid">
				<div className="row p-0">
					<div className="col-md-10 p-0">
						<article className="my-4 px-3">
							<div className="status my-4">
								<h3>
									<strong>Lancamentos:</strong>
								</h3>
								
							</div>

							{(
								<Fragment>
									<table className="table table-striped table-hover mt-3 text-center table-responsive-md w-100">
										<thead className="bg-primary text-white">
											<tr>
                                            <th className="text-center">Data</th>
												<th className="text-center">Tipo</th>
												<th className="text-center">Valor</th>
												<th className="text-center">Conta</th>
												<th className="text-center">Agencia</th>
												<th className="text-center">Descrição</th>
											</tr>
										</thead>
										<tbody>
											{lancamentos.map((lancamento, key) => (
												<LancamentoItem
													key={key}
													lancamento={lancamento}													
												/>
											))}
										</tbody>
									</table>
									
								</Fragment>							
							)}
						</article>
					</div>
				</div>
			</section>
		)
	}
}