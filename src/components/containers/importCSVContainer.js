import {connect} from 'react-redux'
import { ImportCSV } from '../import-csv';
import { bindActionCreators } from 'redux';

import{ImportLancamentos} from '../../actions/conta'

const mapDispatch = disptch => (
    bindActionCreators({ImportLancamentos}, disptch)
)

const ImportCSVContainer = connect(null, mapDispatch)(ImportCSV)

export default ImportCSVContainer