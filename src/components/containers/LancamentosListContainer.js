import { connect } from 'react-redux'
import { LancamentoList } from '../lancamentosList';
import { bindActionCreators } from 'redux';

import { getLancamentos } from "../../actions/conta";

const mapState = state => (
    {
        lancamentos: state.lancamentos.data
    }
)

const mapDispatch = dispatch => (
    bindActionCreators({
        getLancamentos
    }, dispatch)
)

const LancamentosListContainer = connect(mapState, mapDispatch)(LancamentoList)

export default LancamentosListContainer