# Bannet Leads (Nova versão do My Client)

Loja de leads e landingpage do My Client (Bannet leads). Em ReactJS.

## Rodar projeto

Instalar dependências do projeto com npm
```sh
$ npm install
```

Iniciar
```sh
$ npm start
```

## Gerenciamento do projeto

<https://bitbucket.org/bannet/myclient/addon/bitbucket-trello-addon/trello-board>

## Documentação

| Ferramenta | Link |
| ------ | ------ |
| ReactJS | [Documentação](http://facebook.github.io/react-native/docs) |
| Redux | [Documentação](http://redux.js.org/) |
| React Router (para rotas) | [Documentação](https://reacttraining.com/react-router/web/example/basic) |
| Redux Form | [Documentação](https://redux-form.com/7.0.4/docs/gettingstarted.md/) |
| Bootstrap v4 | [Documentação](https://getbootstrap.com/docs/4.0/getting-started/introduction/) |
| Superlógica (pagamentos) | [Documentação](https://superlogicaassinaturas.docs.apiary.io/) |

## Micro serviços

| Micro service | Descrição |
| ------ | ------ |
| Autenticacao | [Retornar tokens(jwt) para usuários válidos](https://bitbucket.org/bannet/bannet-authentication-api) |
| Usuário | [Cadastrar usuarios, Editar informações de usuários](https://bitbucket.org/bannet/bannet-user-api) |
| Leads | [Retornar leads que podem ser compradas](https://bitbucket.org/bannet/bannet-lead-api) |
| Compras leads| [Realizar compras e retornar compras do usuário](https://bitbucket.org/bannet/bannet-billing-api) |
| Pagamentos | Realizar recarga de créditos e retornar histórico de recargas |
| Negativação | [Realizar negativação das leads](https://bitbucket.org/bannet/bannet-negativation-api) |

## Colaboradores

* [Cledson](https://bitbucket.org/cl3dson/)
* [Darlinson Alves](https://bitbucket.org/darlisonalves/)
* [Jeovania](https://bitbucket.org/JeoLopes/)

## TODO

* Versão Premium